from django.conf.urls import patterns, include, url
from django.contrib import admin
from home import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'untitled3.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^edit/', include(admin.site.urls)),
    url(r'^admin/', views.admin, name='admin'),
    url(r'^export_email/', views.export_email, name='export_email'),
    url(r'^search/$', views.search, name='search'),
    url(r'^request/$', views.request, name='request'),
    url(r'^location/$', views.location, name='location'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^register/$',views.register, name='register'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^$', views.index, name='index'),

)